* Source code of [[https://hkdislike.gitlab.io][https://hkdislike.gitlab.io]]

Source code of this site is free software licensed under
MIT Expat (bulma, random-js, underscore)
and GPLv3 or later (other non-data files).
