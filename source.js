// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
// @source https://gitlab.com/hkdislike/hkdislike.gitlab.io/-/blob/master/source.js
// @source https://gitlab.com/hkdislike/dev/-/blob/dev/source.js


const libs =
      {
          // @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
          // @source https://github.com/jashkenas/underscore/blob/1.9.2/underscore.js
          'https://cdn.jsdelivr.net/npm/underscore@1.9.2/underscore-min.js':
          'aSrfdOV2zoMWL9SKC7MKg1BBhB0o2+6ud01BJHfrZntB7q3rEtjZPJeRPHmNbvuN',
          // @license-end

          // @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
          // @source https://unpkg.com/random-js@2.1.0/dist/random-js.umd.js
          'https://unpkg.com/random-js@2.1.0/dist/random-js.umd.min.js':
          'imktNVRyd+AEHpnheJWEqkSb8WKcKBEQ9kVOUZ9H/lk8PagtqRv6ZxYKFLsKNgyB'
          // @license-end
      }


const encoder = new TextEncoder()

const global_eval = str => Function(str)()

const sha384sum =
      str =>
      crypto.subtle.digest('SHA-384', encoder.encode(str))
      .then(arr => btoa(String.fromCharCode(...new Uint8Array(arr))))

// Load source from URL if its hash matched that of HASH
const load =
      (url, hash) =>
      fetch(url)
      .then(res => res.text())
      .then(src => Promise.all([src, sha384sum(src)]))
      .then(([src, hash2]) =>
            hash === hash2
            ? src
            : Promise.reject(Error('load: ' + url + ' should have SHA-384 ' + hash + ' instead of ' + hash2)))
      .then(global_eval)

const load_all = libs => Promise.all(Object.entries(libs)
                                     .map(entry => load(...entry)))


const main =
      () =>
      {const rand = new Random.Random(Random.browserCrypto)

       // rand_int ~ Unif({0, 1, ..., n - 1})
       const rand_int = n => rand.integer(0, n - 1)

       // split string into an array of nonempty lines
       const lines = str => _.compact(str.split('\n'))

       // get text from file or blob
       const get_text =
             x => new Promise (cont => {const rdr = new FileReader()
                                        rdr.onload = () => cont(rdr.result)
                                        rdr.readAsText(x)})

       const get_elt_by_cls =
             name => _.first(document.getElementsByClassName(name))

       const get_elt_by_id = id => document.getElementById(id)

       const hide = elt => elt.classList.add('is-hidden')
       const show = elt => elt.classList.remove('is-hidden')

       const get_data =
             (url_prefix, file_name) =>
             _.isEqual(window.location.protocol, 'file:')
             ? new Promise (cont => {show(get_elt_by_cls('file'))
                                     get_elt_by_cls('file-input').onchange =
                                     val => {hide(get_elt_by_cls('file'))
                                             cont(_.first(val.target.files))}})
             : fetch(url_prefix + '/' + file_name).then(data => data.blob())

       const url_prefix = window.location.href.replace(/[^\/]+\.html$/, '')
       const file_name = get_elt_by_cls('file-name').innerHTML

       const num_of_vids_to_be_disliked =
             parseInt(get_elt_by_id('num-of-vids-to-be-disliked').value)

       get_data(url_prefix, file_name)
       .then(get_text)
       .then(lines)
       .then(all_vid_ids =>
             _.reduce(_.range(num_of_vids_to_be_disliked),
                      (vid_ids, k) =>
                      {const x = rand_int(_.size(vid_ids)),
                             vid_id = vid_ids[x],
                             page = 'https://developers.google.com/youtube/v3/docs/videos/rate?apix=true&apix_params=%7B%22id%22%3A%22' + vid_id + '%22%2C%22rating%22%3A%22dislike%22%7D#try-it_1',
                             thumbnail = 'https://i.ytimg.com/vi/' + vid_id + '/default.jpg',
                             elt_id = 'box' + k.toString()

                       get_elt_by_id(elt_id).href = page
                       get_elt_by_id(elt_id).style.backgroundImage += ',url(' + thumbnail + ')'
                       show(get_elt_by_id(elt_id))

                       return _.without(vid_ids, vid_id)},
                      all_vid_ids))}


load_all(libs)
    .then(main)
    .catch(console.log)


// @license-end
